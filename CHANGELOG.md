# CHANGELOG

<!--- next entry here -->

## 0.2.0
2022-01-24

### Features

- server naming (656629c4e14ce3717d8232c44876d457430552e5)

## 0.1.1
2022-01-24

### Fixes

- add push to github (e78b6e5b1c97a1f4c5ce2a362848697c73de2995)

## 0.1.0
2022-01-24

### Features

- initial commit (a66e7af6d3d68a970988b67652b20a5305cd5931)

### Fixes

- initial (eedba0a7ccc68b92c1da7ea28734bbec5c499b06)
- fix stages (9da98fa4bf9e4126a550fd14af71a8277bde93bd)
- fix stages (508fe40407fdb9edffdddcec731b03e2db4c390a)
- removed ulimit (7a285c0d3c83ae8496bab5cd1895cfc54598da70)
- set admin password on start-server.sh (5e2e7a5ea3c20b33e6aca8566a4c072f7b182eaf)
- remove github push (188caff1c475f02c1c88fc652ff371bf74ed59e6)