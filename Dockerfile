FROM ubuntu

LABEL Author="JFWenisch"

RUN apt-get update
RUN apt-get -y install lib32gcc1 libc6-i386 wget bash

ENV DATA_DIR="/serverdata"
ENV STEAMCMD_DIR="${DATA_DIR}/steamcmd"
ENV SERVER_DIR="${DATA_DIR}/serverfiles"
ENV GAME_ID="380870"
ENV GAME_NAME="project-zomboid"
ENV SERVER_NAME="AuHuur Gaming"
ENV GAME_PARAMS="+game_type 0 +game_mode 0 +mapgroup mg_active +map de_dust2"
ENV GAME_PORT=27015

RUN mkdir $DATA_DIR
RUN mkdir $STEAMCMD_DIR
RUN mkdir $SERVER_DIR
RUN mkdir /scripts
# RUN wget -q -O ${STEAMCMD_DIR}/steamcmd_linux.tar.gz http://media.steampowered.com/client/steamcmd_linux.tar.gz \
#  &&  tar --directory ${STEAMCMD_DIR} -xvzf ${STEAMCMD_DIR}/steamcmd_linux.tar.gz \
#  &&  rm ${STEAMCMD_DIR}/steamcmd_linux.tar.gz \
#  &&  chmod -R 774 $STEAMCMD_DIR  $SERVER_DIR 

COPY scripts/start-server.sh /scripts/start-server.sh
RUN chmod -R 774 /scripts/

#Server Start
ENTRYPOINT ["/scripts/start-server.sh"]
